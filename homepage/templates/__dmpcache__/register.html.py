# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1555646483.9631028
_enable_loop = True
_template_filename = '/home/christian/Desktop/personal/creative/homepage/templates/register.html'
_template_uri = 'register.html'
_source_encoding = 'utf-8'
import django_mako_plus
import django.utils.html
_exports = ['content']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, 'base.htm', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        def content():
            return render_content(context._locals(__M_locals))
        csrf_input = context.get('csrf_input', UNDEFINED)
        self = context.get('self', UNDEFINED)
        request = context.get('request', UNDEFINED)
        form = context.get('form', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\n\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        __M_writer('\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def content():
            return render_content(context)
        csrf_input = context.get('csrf_input', UNDEFINED)
        self = context.get('self', UNDEFINED)
        request = context.get('request', UNDEFINED)
        form = context.get('form', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\n    <header class="container home-header justify-center">\n')
        if not request.user.is_authenticated:
            __M_writer('        <div class="col-md-6 offset-3">\n            <form method="POST">\n                ')
            __M_writer(django_mako_plus.ExpressionPostProcessor(self)(csrf_input))
            __M_writer('\n                ')
            __M_writer(django_mako_plus.ExpressionPostProcessor(self)(form))
            __M_writer('\n                <button type="submit" class="btn btn-info">Sign Up</button>\n            </form>\n        </div>\n')
        else:
            __M_writer('        <h1>You are logged in, log out to register a new account\n        </h1>\n')
        __M_writer('    </header>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"filename": "/home/christian/Desktop/personal/creative/homepage/templates/register.html", "uri": "register.html", "source_encoding": "utf-8", "line_map": {"29": 0, "40": 1, "45": 18, "51": 3, "61": 3, "62": 5, "63": 6, "64": 8, "65": 8, "66": 9, "67": 9, "68": 13, "69": 14, "70": 17, "76": 70}}
__M_END_METADATA
"""

# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1555684664.4521534
_enable_loop = True
_template_filename = '/home/christian/Desktop/personal/creative/homepage/templates/login.html'
_template_uri = 'login.html'
_source_encoding = 'utf-8'
import django_mako_plus
import django.utils.html
_exports = ['content']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, 'base.htm', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        def content():
            return render_content(context._locals(__M_locals))
        self = context.get('self', UNDEFINED)
        csrf_input = context.get('csrf_input', UNDEFINED)
        form = context.get('form', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\n\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        __M_writer('\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def content():
            return render_content(context)
        self = context.get('self', UNDEFINED)
        csrf_input = context.get('csrf_input', UNDEFINED)
        form = context.get('form', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\n    <header class="container home-header justify-center">\n        <div class="col-md-6 offset-3">\n            <form method="POST">\n                ')
        __M_writer(django_mako_plus.ExpressionPostProcessor(self)(csrf_input))
        __M_writer('\n                ')
        __M_writer(django_mako_plus.ExpressionPostProcessor(self)(form))
        __M_writer('\n                <button type="submit" class="btn btn-info">Log In</button>\n            </form>\n            <h3>Not a member? Sign up <a href="/register">here</a></h3>\n        </div>\n    </header>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"filename": "/home/christian/Desktop/personal/creative/homepage/templates/login.html", "uri": "login.html", "source_encoding": "utf-8", "line_map": {"29": 0, "39": 1, "44": 14, "50": 3, "59": 3, "60": 7, "61": 7, "62": 8, "63": 8, "69": 63}}
__M_END_METADATA
"""

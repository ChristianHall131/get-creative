# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1556292198.9752698
_enable_loop = True
_template_filename = '/home/christian/Desktop/personal/creative/homepage/templates/addcreation.html'
_template_uri = 'addcreation.html'
_source_encoding = 'utf-8'
import django_mako_plus
import django.utils.html
_exports = ['content']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, 'base.htm', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        csrf_input = context.get('csrf_input', UNDEFINED)
        self = context.get('self', UNDEFINED)
        HttpResponseRedirect = context.get('HttpResponseRedirect', UNDEFINED)
        def content():
            return render_content(context._locals(__M_locals))
        request = context.get('request', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\n\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        __M_writer('\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        csrf_input = context.get('csrf_input', UNDEFINED)
        self = context.get('self', UNDEFINED)
        HttpResponseRedirect = context.get('HttpResponseRedirect', UNDEFINED)
        def content():
            return render_content(context)
        request = context.get('request', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\n')
        if request.user.is_authenticated:
            __M_writer('    <div>\n        <form method="POST">\n            ')
            __M_writer(django_mako_plus.ExpressionPostProcessor(self)(csrf_input))
            __M_writer('\n            <input type=\'text\' placeholder="Title of Creation" name=\'title\' />\n            <input type=\'text\' placeholder="Description of Creation" name=\'description\' />\n            <select name="category" onchange="forminput(this.value)">\n                <option value="t">Text (Story, Poem, Ect.)</option>\n                <option value="i">Image (Drawing,Painting,Ect)</option>\n            </select>\n            <input id="text" type=\'text\' placeholder="Text" name=\'text\' style="display:none;" />\n            <input id="image" type=\'file\' accept="image/*" placeholder="Text" name=\'image\' style="display:none;" />\n            <button type=\'submit\' enabled=\'false\'>Share your Creation!</button>\n\n\n        </form>\n    </div>\n    <script>\n        function forminput(category){\n            if(category === "t"){\n                var text = document.getElementById("text")\n                var image = document.getElementById("image")\n                image.style.display = "none";\n                text.style.display = "block";\n                text.value = ""\n                image.value = ""\n            }\n            else if(category === "i"){\n                var text = document.getElementById("text")\n                var image = document.getElementById("image")\n                image.style.display = "block";\n                text.style.display = "none"; \n                text.value = ""\n                image.value = ""\n            }\n        }\n    </script>\n')
        else:
            __M_writer('    ')
            __M_writer(django_mako_plus.ExpressionPostProcessor(self)(HttpResponseRedirect("/login")))
            __M_writer('\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"filename": "/home/christian/Desktop/personal/creative/homepage/templates/addcreation.html", "uri": "addcreation.html", "source_encoding": "utf-8", "line_map": {"29": 0, "40": 1, "45": 44, "51": 3, "61": 3, "62": 4, "63": 5, "64": 7, "65": 7, "66": 41, "67": 42, "68": 42, "69": 42, "75": 69}}
__M_END_METADATA
"""

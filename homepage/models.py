from django.db import models
from django.contrib.auth.models import AbstractUser
# Create your models here.
class User(AbstractUser):
    unique_id = models.CharField(max_length=64)
    user_description = models.CharField(null=True, blank = True, max_length=1000)

class Comments(models.Model):
    COMMENT_TYPE = (
        ('r','regular'),
        ('c','constructive')
    )
    owner = models.ForeignKey(User,on_delete=models.DO_NOTHING)
    text = models.CharField(max_length=500,blank=True,null=False)
    type = models.CharField(choices=COMMENT_TYPE,default='r',max_length=16)

class Media(models.Model):
    CHOICES = (
        ('i',"image"),
        ('s','sound'),
        ('t','text'),
        ('v','video'),
        ('n','none'),
    )
    owner = models.ForeignKey(User, on_delete=models.DO_NOTHING,null=True)
    media_id = models.CharField(max_length=64, blank=False,null=False)
    name = models.CharField(max_length = 112, blank=True, null=True)
    description = models.CharField(max_length=1000,blank=True,null=True)
    comments = models.ManyToManyField(Comments,null=True)
    media_type = models.CharField(max_length=16,choices=CHOICES,default="n")
    media_text = models.CharField(max_length = 10000,blank=True,null=True)
    image_id = models.CharField(max_length=32)


from django.conf import settings
from django_mako_plus import view_function, jscontext
from datetime import datetime, timezone
from django import forms
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, login
from homepage.models import User,Media
import random
import string
import cloudinary

def getrandom(len):
    key = ''.join(random.SystemRandom().choice(string.ascii_lowercase+string.digits)for x in range(len))
    return(key)

@view_function

def process_request(request):
    if request.method == "POST":
        media = Media()
        media.owner = request.user
        media.name = request.POST.get('title')
        media.description = request.POST.get('description')
        media.media_id = getrandom(32)
        if request.POST.get('category') == "i":
            media.image_id = getrandom(16)
            media.media_type = "i"
        if request.POST.get('category') == "t":
            media.media_text = request.POST.get('text')
            media.media_type = request.POST.get('category')
        media.save()
    utc_time = datetime.utcnow()
    context = {
        # sent to index.html:
        'utc_time': utc_time,
        # sent to index.html and index.js:
        jscontext('utc_epoch'): utc_time.timestamp(),
    }
    return request.dmp.render('addcreation.html', context)
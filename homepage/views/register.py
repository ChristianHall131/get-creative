from django.conf import settings
from django_mako_plus import view_function, jscontext
from datetime import datetime, timezone
from django import forms
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, login
from homepage.models import User
import random
import string

class SignupForm(forms.Form):
    first_name = forms.CharField(label = "First Name", required = True,widget=forms.TextInput({'placeholder':'First Name', 'class':'field'}))
    last_name = forms.CharField(label="Last Name", required=True,widget=forms.TextInput({'placeholder':'Last Name', 'class':'field'}))
    username = forms.CharField(label="Username", required=True,widget=forms.TextInput({'placeholder':' Username', 'class':'field'}))
    email = forms.EmailField(label="Email",required=True,widget=forms.TextInput({'placeholder':'Email', 'class':'field'}))
    description = forms.CharField(label="Describe Yourself")
    passwordcheck = forms.CharField(label='Password', required=True, max_length=100, widget=forms.PasswordInput({ "placeholder": "Enter Password","class":"field"}))
    password = forms.CharField(label='Repeat Password', required=True, max_length=100, widget=forms.PasswordInput({ "placeholder": "Re-enter Password","class":"field"}))
    
    def clean_username(self):
        username = self.cleaned_data.get("email")
        try:
            user = User.objects.get(username=self.cleaned_data.get("username"))
            raise forms.ValidationError("This email is already taken")
            print(user, "USER OBJECT CLEANED")
        except User.DoesNotExist:
            print("DOESN'T EXIST")
            pass
        return username
    def clean_password(self):
        print("CLEANING PASSWORDS", self.cleaned_data.get("password"),self.cleaned_data.get("passwordcheck"), self.cleaned_data)
        if self.cleaned_data.get("password") != self.cleaned_data.get("passwordcheck"):
            raise forms.ValidationError("Your passwords need to be the same")
        return self.cleaned_data

def getrandom():
    key = ''.join(random.SystemRandom().choice(string.ascii_lowercase+string.digits)for x in range(31))
    return(key)

@view_function

def process_request(request):
    form = SignupForm()
    if request.method == "POST":
        form = SignupForm(request.POST)
        if form.is_valid():
            # u = User.objects.create_user(username = form.data.get("username"),first_name=form.data.get("first_name"),last_name=form.data.get("last_name"),email = form.data.get("email"),password = form.cleaned_data.get("password"),user_description = form.data.get("description"),unique_id = getrandom())
            # print("OUR FORM DATA--------------",form.data)
            # u.is_active=True
            # u.save()
            u = User()
            u.username = request.POST.get('username', '')
            u.set_password(request.POST.get('password', ''))
            u.unique_id = getrandom()
            u.first_name = request.POST.get('first', '')
            u.last_name = request.POST.get('last', '')
            u.email = request.POST.get('email', '')
            u.user_description = request.POST.get('description','')
            u.save()
            user = authenticate(username=request.POST.get('username', ''), password=request.POST.get('password', ''))
            login(request, user)
            # try:
            #     password=(str(request.POST.get("password")))
            #     print("password id",password,u.username,u.password)   
            #     user = authenticate(username=u.username,password=password)                
            #     print("AUTHENTICATED++++++++++++++++",user)
            # # print(user.username,user.password,"is user")
            #     login(request,user)
            #     return HttpResponseRedirect("/")
            # except:
            #     print("You're still getting errors")
            
    utc_time = datetime.utcnow()
    context = {
        # sent to index.html:
        'utc_time': utc_time,
        'form': form,
        # sent to index.html and index.js:
        jscontext('utc_epoch'): utc_time.timestamp(),
    }
    return request.dmp.render('register.html', context)
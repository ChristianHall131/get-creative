from django.conf import settings
from django_mako_plus import view_function, jscontext
from datetime import datetime, timezone
from django import forms
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, login
from homepage.models import User
import random
import string

class LoginForm(forms.Form):
    username = forms.CharField(required = True,widget=forms.TextInput({'placeholder':'Username', 'class':'field'}))
    password = forms.CharField(required = True,widget=forms.TextInput({'placeholder':'Password', 'class':'field'}))



@view_function

def process_request(request):
    form = LoginForm()
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            print(form.data)
            user = authenticate(username = request.POST.get('username', ''), password = request.POST.get('password', ''))
            print("AUTHENTICATED++++++++++++++++",user)
            # print(user.username,user.password,"is user")
            login(request,user)
            return HttpResponseRedirect("/")
            
    utc_time = datetime.utcnow()
    context = {
        # sent to index.html:
        'utc_time': utc_time,
        'form': form,
        # sent to index.html and index.js:
        jscontext('utc_epoch'): utc_time.timestamp(),
    }
    return request.dmp.render('login.html', context)